# DevOps Software Stack

## DevOps common workflow

- Push source code to **Repository**
- Static analysis ( Coding review ) 
- Pre-deployment testing 
- Packaging and deployment
- Post-deployment testing
	
## Development tool

### IDE
- **Eclipse** for coding Java backend service.  ( FREE ) 
- **Microsoft visualstudio code** for coding UX/UI  ( FREE ) 

### Static analysis tool
- **Checkstyle, Findbugs** For analysis Java source code quality. ( FREE )
- **ESLint, JSLint** For analysis Javascript source code quality. ( FREE )

### Dependencies management & Packaging tool
- **Apache Maven** For control dependencies & packaging in Java. ( FREE )
- **NPM** Node Package Manager for control Javascript dependencies. ( FREE ) 

### Automated Testing tool
**Backend services**
- **JUnit** For perform unit testing Java application.
- **JMeter** For perform loadtest Java services. 

**UX/UI**
- **Robot Framework** 

### Repository and Registry
**Source code repository**
- **Gitlab.com** Free cloud hosting with unlimited private project and team member.

**Artifact repository**
- **Nexus** Nexus can be hosted any format of artifact ( Java, npm, Python..) (FREE software required install on central server)

**Container registry**
- **Nexus** Nexus can also hostd Docker containers.( FREE but required installation ) 
- **Gitlab.com** Always free and private on cloud.

### CI / CD tool
- **Jenkins** Jenkins is free software required installation on central server

### Centrallized logging and monitoring 
- **ELK stack**  Logstash, Elastics, and Kibana

### Project management & Tracker
- **Jira software** Agile project management tool.  Pricing depend on number of team member.

|Users|JIRA Software Cloud Annual|
|:--|--:|
|1 - 10|	$100|
|11 - 15|	$750|
|16 - 25|	$1,500|
|26 - 50|	$3,000|
|51- 100|	$4,500|
|101 - 500|	$7,500|
|501 - 2000|	$15,000|


